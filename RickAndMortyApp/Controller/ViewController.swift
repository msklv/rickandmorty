//
//  ViewController.swift
//  RickAndMortyApp
//
//  Created by Павел Москалёв on 15.05.21.
//

import UIKit

class ViewController: UIViewController {

    var networkManager = NetworkManager()
    var unique: [Result] = []
    
    
    @IBOutlet weak var tableView: UITableView! {
        didSet{
            tableView.dataSource = self
            tableView.delegate = self
            let nib = UINib(nibName: CharacterTableViewCell.nib, bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: CharacterTableViewCell.identifier)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchAllCharacters()
 }
    
    func fetchAllCharacters() {
        networkManager.fetchCharacter { [weak self] (model) in
            guard let self = self else { return }
            self.unique.append(contentsOf: model?.results ?? [])
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            }
    }
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return unique.count
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: CharacterTableViewCell.identifier, for: indexPath) as! CharacterTableViewCell
    cell.configure(unique[indexPath.row])
    return cell
}
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 126
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "DetailInfoStoryBoard", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "detail") as! DetailViewController
        vc.detailCharacters = unique[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }

}
