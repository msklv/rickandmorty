//
//  CharacterTableViewCell.swift
//  RickAndMortyApp
//
//  Created by Павел Москалёв on 15.05.21.
//

import UIKit
import Kingfisher

class CharacterTableViewCell: UITableViewCell {
    
    @IBOutlet weak var characterImage: UIImageView!
    
    @IBOutlet weak var characterStatusLabel: UILabel!
    @IBOutlet weak var characterNameLabel: UILabel!
    
    static let nib = "CharacterTableViewCell"
    static let identifier = "characterCell"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(_ result: Result?) {
        guard let result = result  else { return }
        if let url = URL(string: result.image ?? " ") {
                self.characterImage?.kf.indicatorType = .activity
                self.characterImage?.kf.setImage(with: url)
        }
        self.characterNameLabel.text = result.name
        self.characterStatusLabel.text = result.status
        switch result.status {
        case "Alive":
            characterStatusLabel.textColor = .green
        case "Dead":
            characterStatusLabel.textColor = .red
        default:
            characterStatusLabel.textColor = .black
            
        }

    }

}
