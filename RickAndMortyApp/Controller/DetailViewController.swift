//
//  DetailViewController.swift
//  RickAndMortyApp
//
//  Created by Павел Москалёв on 19.05.21.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var characterImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var speciesLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    var detailCharacters: Result?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print(detailCharacters)
    }
    
    func setUpView() {
        nameLabel.text = "Name - \(detailCharacters?.name ?? "")"
        statusLabel.text = "Status - \(detailCharacters?.status ?? "")"
        speciesLabel.text = "Species - \(detailCharacters?.species ?? "")"
        genderLabel.text = "Gender - \(detailCharacters?.gender ?? "")"
        locationLabel.text = "Location - \(detailCharacters?.location?.name ?? "")"
        
        if let url = URL(string: detailCharacters?.image ?? " ") {
            characterImageView.kf.indicatorType = .activity
            characterImageView.kf.setImage(with: url)
        }
        
        switch detailCharacters?.status {
        case "Alive":
            statusLabel.textColor = .green
        case "Dead":
            statusLabel.textColor = .red
        default:
            statusLabel.textColor = .black
    }
}

}
