//
//  CellViewModel.swift
//  RickAndMortyApp
//
//  Created by Павел Москалёв on 16.05.21.
//

import Foundation
class CellViewModel {

var date: AllCharactersModel?

var name : String? {
    date?.results?[0].name
}

var image : String? {
    date?.results?[0].image
}

}
