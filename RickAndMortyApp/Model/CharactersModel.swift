//
//  CharactersModel.swift
//  RickAndMortyApp
//
//  Created by Павел Москалёв on 15.05.21.
//

import Foundation

struct AllCharactersModel: Codable {
    let results: [Result]?
}

// MARK: - Result
struct Result: Codable {
    let id: Int?
    var name: String?
    var status: String?
    let species: String?
    var gender: String?
    let origin: Location?
    var location: Location?
    var image: String?
}

// MARK: - Location
struct Location: Codable {
    var name: String?
}
