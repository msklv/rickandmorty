//
//  NetworkManager.swift
//  RickAndMortyApp
//
//  Created by Павел Москалёв on 15.05.21.
//

import Foundation

class NetworkManager {
    
    func fetchCharacter(complition : @escaping(AllCharactersModel?) -> Void){
        if let url = URL(string: "https://rickandmortyapi.com/api/character") {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
                guard self != nil else { return }
                if let data = data {
                    let characterData = try? JSONDecoder().decode(AllCharactersModel.self, from: data)
                    complition(characterData)
                } else {
                    DispatchQueue.main.async {
                        print("NO INTERNET!!!!!!!")
                    }
                }
            }
            task.resume()
        }
    }
}
